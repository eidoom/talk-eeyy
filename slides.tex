\documentclass{beamer}
\input{preamble}

\newcommand{\talktitle}{$e^+e^-\to\gamma\gamma^*$ at two loops in massless QED}

\title{\href{https://indico.psi.ch/event/13708/contributions/43409/}{\talktitle}}

\author{Ryan Moodie}

\institute[Turin]{Turin University}

\date{
    \href{https://indico.psi.ch/event/13708/}{Radiative corrections and Monte Carlo tools for low-energy hadronic cross sections in $e^+e^-$ collisions}\\
    \vspace{1em}
    \href{https://indico.psi.ch/event/13708/sessions/7794/\#20230608}{Loops in QED}\\
    \vspace{1em}
    8 Jun 2023
}

\titlegraphic{
    \begin{tikzpicture}[x=7em]
        \def\l{3em}
        \node at (0,0) {
            \href{https://home.infn.it/it/}{\includegraphics[width=\l,height=\l,keepaspectratio]{infn_logo}}
        };
        \node at (1,0) {
            \href{https://www.unito.it/}{\includegraphics[width=\l,height=\l,keepaspectratio]{torino_logo}}
        };
        \node at (2,0) {
            \href{https://cordis.europa.eu/project/id/772099}{\includegraphics[width=\l,height=\l,keepaspectratio]{erc_logo}}
        };
    \end{tikzpicture}
}

\begin{document}

\begingroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
    \titlepage
\end{frame}
\endgroup

\section*{Outline}

\begin{frame}{Outline}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{minipage}[t][0.6\textheight]{0.9\textwidth}
                \vspace{-3ex}
                \raggedright\tableofcontents
            \end{minipage}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{art}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section[Introduction]{Introduction:\\\hspace{1em}precision frontier}

\begin{frame}[fragile]{Q*D amplitudes}{Recent analytic results}
    \asyfull{amplitude-complexity}
\end{frame}

\begin{frame}[fragile]{Matrix elements}{Fixed order}
    \asyfull{fixed-order}
\end{frame}

\section[Amplitudes]{Amplitudes:\\\hspace{1em}computation and result}

\begin{frame}{Computation}{Summary}
    \begin{align*}
        \mathcal{A}(x) &= \sum_i \tikzmarknode{c}{c_i(x)} \tikzmarknode{f}{f_i(x)}
    \end{align*}
    \vspace{1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item \tikzmarknode{cc}{Rational coefficients}
                \item Reconstruct compact analytic form from numerical evaluations over finite fields
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item \tikzmarknode{ff}{Feynman integrals}
                \item ``Special function'' basis of Goncharov polylogarithms (GPLs)
            \end{itemize}
        \end{column}
    \end{columns}
    \tikz[remember picture, overlay] \draw[->] ([yshift=-4pt] c.south) to[out=-90,in=90] ([yshift=+4pt] cc.north);
    \tikz[remember picture, overlay] \draw[->] ([yshift=-4pt] f.south) to[out=-90,in=90] ([yshift=+4pt] ff.north);
\end{frame}

\begin{frame}{Result}{\pcite{!paper}}
    \begin{itemize}
        \item Helicity amplitudes
        \item Current $\mathcal{A}^\mu(h_1,h_2,h_3)$
            \asyauto{current}{0.4}{1}
        \item Running in \texttt{McMule} ($e\mu \to e\mu \gamma$) at $\sim$130\,ev/s~\pcite{Durham:n3lo}
        \item Massification~\pcite{Penin:2005eh,Becher:2007cu,Engel:2018fsb}
    \end{itemize}
\end{frame}

\section[Phenomenology]{Phenomenology:\\\hspace{1em}$a_\mu^{\text{HVP}}$}

\begin{frame}{Measurements}{See other talks!}
    \begin{itemize}
        \item $a_\mu$
            \begin{itemize}
                \item most theoretical uncertainty: $a_\mu^{\text{HVP}}$~\pcite{Abbiendi:2022liz}
                \item $ee$ (timelike)
                    \begin{itemize}
                        \item dispersive integral method~\pcite{Aoyama:2020ynm}
                        \item requires $\sigma(ee\to\text{hadrons}/\mu\mu(+\gamma))$
                        \item data-driven $\gamma^*\to\text{hadrons}$~\pcite{!precisionsm}
                        \item amplitude ingredient $ee\to\gamma^*(+\gamma)$
                    \end{itemize}
                \item $e\mu$ (spacelike)
                    \begin{itemize}
                        \item $e\mu\to e\mu$
                        \item MUonE~\pcite{Abbiendi:2016xup,Balzani:2021del}
                    \end{itemize}
            \end{itemize}
        \item $a_\tau$
    \end{itemize}
\end{frame}

\begin{frame}{Corrections}
    \vspace{-1em}
    \begin{columns}[T]
        \begin{column}{0.33\textwidth}
            \begin{center}
                {\large Direct scan}\\
                \vspace{1ex}
                $ee\to\gamma^*$
            \end{center}
            \asyauto{fsr}{0.75}{0.25}
            \begin{itemize}
                \centering
                \item Scan over $\sqrt{s}$
                \item Initial-state corrections
                \item RVV for \nnnlo
            \end{itemize}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{center}
                {\large Radiative return}\\
                \vspace{1ex}
                $ee\to\gamma\gamma^*$
            \end{center}
            \asyauto{isr}{0.75}{0.25}
            \begin{itemize}
                \centering
                \item Scan over ISR
                \item Initial-state corrections
                \item VV for NNLO
            \end{itemize}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{center}
                {\large MUonE}\\
                \vspace{1ex}
                $e\to e\gamma^*$
            \end{center}
            \asyauto{muone}{0.75}{0.25}
            \begin{itemize}
                \centering
                \item Electron-line corrections
                \item RVV for \nnnlo
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion}
    \begin{itemize}
        \item Massless $ee\to\gamma\gamma^*$ two-loop helicity amplitude currents
            \vspace{1ex}
        \item Fast and stable evaluation
            \begin{itemize}
                \item Compact analytic form
                \item GPL integral basis
            \end{itemize}
            \vspace{1ex}
        \item Access next order
            \begin{itemize}
                \item RVV for \nnnlo~$ee\to\gamma^*$ and $e\to e\gamma^*$
                \item VV for NNLO $ee\to\gamma\gamma^*$
            \end{itemize}
    \end{itemize}
\end{frame}

\begingroup
\miniframesoff
\tocfootline
\begin{frame}[allowframebreaks,noframenumbering]{Bibliography}
    \printbibliography[heading=none]
\end{frame}
\endgroup

\nocite{!src}
\end{document}
