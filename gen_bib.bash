#!/usr/bin/env bash

file=slides.tex
tmpf=tmp.tex

grep -o "cite{[a-zA-Z0-9:,\-\!]*}" $file | sed 's/^/\\/' > $tmpf

for f in asy/*.asy; do
	grep -o "cite{[a-zA-Z0-9:,\-\!]*}" $f | sed 's/^/\\/' >> $tmpf
done

json=$(curl -XPOST -F "file=@$tmpf" "https://inspirehep.net/api/bibliography-generator?format=bibtex")

rm $tmpf

if [[ $json == *"status"* ]]; then
	echo $json
	exit 1
else
	echo "InspireHEP API call successful"
fi

# TODO Alpine doesn't have `grep -P`, rewrite to support CI image
# errors=$(echo $json | grep -Po '"errors":\[.*?\]' | cut -c 10-)

# if [ "$errors" != "[]" ]; then
# 	echo "Errors: $errors"
# 	exit 1
# else
# 	echo "No errors"
# fi

url=$(echo $json | perl -pe 's/.*"download_url":"(.*?)".*/\1/g')

curl $url > bibliography.bib

sed -i 's/Moodie,\s*Ryan/\\textbf{RM}/g' bibliography.bib

sed -i 's/\\^\\i{}/i/g' bibliography.bib
