import feynman;
import patterns;

int l = 30;

pair a0 = (0,0),
     a2 = (2*l,0),
     b0 = (0,-l),
     b2 = (2*l,-l),
     c0 = a0+l*dir(135),
		 c1 = b0+l*dir(225),
		 c2 = b2+l*dir(315),
		 c3 = a2+l*dir(45);

currentpen = linewidth(0.5);
pen massive = linewidth(2);
arrowbar arrow = MidArrow(TeXHead, 1.5);

drawFermion(c0--a0, arrow);
drawFermion(b0--c1, arrow);

pair aa = a2 + l/sqrt(2);
drawPhoton(a2--aa, erasebg=false, massive);
drawPhoton(b2--c2, erasebg=false);

pair d = aa + l/4,
	e = d + l/4,
	f = e + l/sqrt(2),
	g = f + l*dir(45),
	h = f + l*dir(315);

label("$\cdot$", d);

drawPhoton(e--f, massive);
drawFermion(f--g, arrow);
drawFermion(h--f, arrow);
drawVertex(f);

add("hatch",hatch(1.5mm));
path blob = shift(l,-l/2)*scale(1.2*l, 0.9*l)*unitcircle;
filldraw(blob, white);
fill(blob, pattern("hatch"));

filldraw(shift(l/2,-l/2)*scale(1/3*l)*unitcircle, white);
filldraw(shift(l*3/2,-l/2)*scale(1/3*l)*unitcircle, white);

label("$e^-_{h_1}$", c0, NW);
label("$e^+_{h_2}$", c1, SW);
label("$\gamma_{h_3}$", c2, SE);
label("$\gamma^*$", d, N);
label("$f_{h_5}$", g, NE);
label("$\bar{f}_{h_4}$", h, SE);
