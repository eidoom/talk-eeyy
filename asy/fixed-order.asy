import feynman;

unitsize(65, 45);

int x0 = 0,
    x1 = 3,
    y0 = 0,
    y1 = 3;

int xl = x1 - x0 + 2,
    yl = y1 - y0 + 2;

arrowbar arrow = Arrow(TeXHead);

draw((xl/4,1/4) .. (3*xl/4, 1/4), arrow);
draw((1/4,yl/4) .. (1/4, 3*yl/4), arrow);

pair graph_coord(real x, real y) {
  return (x - x0 + 1, y - y0 + 1);
}

label(rotate(90) * "virtual (loops)", (0, yl / 2));
label("real (legs)", (xl / 2, 0));

arrowbar arrow = MidArrow(TeXHead);
pen massive = linewidth(1.5);
int l = 20, w = 25;
pair o = (0, 0);

real outer_radius(int l, int n) {
	return l / 2 / Sin(360 / n / 2);
}

real inner_radius(int l, int n) {
	return l / 2 / Tan(360 / n / 2);
}

pair polygon_vertex(int i, int n, int r = 0) {
	return dir(i * 360 / n + r);
}

pair[] polygon_vertices(int l, int n, int r = 0) {
	return sequence(new pair(int i) { return outer_radius(l, n) * polygon_vertex(i, n, r); }, n);
}

pair[] p3i = polygon_vertices(l, 3, 0),
    p3o = scale(2)*p3i,
    p4i = polygon_vertices(l, 4, 45),
    p4o = scale(2)*p4i,
    p4il = p4i - (inner_radius(l, 3) + inner_radius(l, 4)),
    p4ol = p4o - (inner_radius(l, 3) + inner_radius(l, 4)),
    p4ill = p4il - (inner_radius(l, 3) + inner_radius(l, 4)),
    p4oll = p4ol - (inner_radius(l, 3) + inner_radius(l, 4)),
    p4ia = p4i - 2*inner_radius(l, 4),
    p4oa = p4o - 2*inner_radius(l, 4),
    p5i = polygon_vertices(l, 5, 72),
    p5o = scale(2) * p5i;

picture B;
size(B, w);
drawFermion(B, p3i[1]--o, arrow);
draw(B, o--p3i[2]);
drawPhoton(B, o--p3i[0], erasebg=false, massive);

picture R;
size(R, w);
drawFermion(R, (-l,l/2)--(0,l/2)--(0,-l/2)--(-l,-l/2), arrow);
drawPhoton(R, (0,l/2)--(l,l/2), erasebg=false, massive);
drawPhoton(R, (0,-l/2)--(l,-l/2), erasebg=false);

picture RR;
size(RR, w);
drawFermion(RR, (-l,l/2)--(0,l/2), arrow);
draw(RR,(0,l/2)--(0,-l/2)--(-l,-l/2));
drawPhoton(RR, (0,l/2)--(l,l/2), erasebg=false, massive);
drawPhoton(RR, (0,0)--(l,0), erasebg=false);
drawPhoton(RR, (0,-l/2)--(l,-l/2), erasebg=false);

picture RRR;
size(RRR, w);
drawFermion(RRR, (-l,l/2)--(0,l/2), arrow);
draw(RRR,(0,l/2)--(0,-l/2)--(-l,-l/2));
drawPhoton(RRR, (0,l/2)--(l,l/2), erasebg=false, massive);
drawPhoton(RRR, (0,l/6)--(l,l/6), erasebg=false);
drawPhoton(RRR, (0,-l/6)--(l,-l/6), erasebg=false);
drawPhoton(RRR, (0,-l/2)--(l,-l/2), erasebg=false);

picture V;
size(V, w);
draw(V, p3o[1]--p3i[1]);
drawFermion(V, p3i[1]--p3i[0], arrow);
draw(V, p3i[0]--p3i[2]--p3o[2]);
drawPhoton(V, p3i[1]--p3i[2], erasebg=false);
drawPhoton(V, p3i[0]--p3o[0], erasebg=false, massive);

picture RV;
size(RV, w);
drawFermion(RV, p4o[1]--p4i[1]--p4i[0]--p4i[3]--p4i[2]--p4o[2], erasebg=false, arrow);
drawPhoton(RV, p4i[1]--p4i[2], erasebg=false);
drawPhoton(RV, p4i[0]--p4o[0], erasebg=false, massive);
drawPhoton(RV, p4i[3]--p4o[3], erasebg=false);

picture RRV;
size(RRV, w);
drawFermion(RRV, p5o[1]--p5i[1]--p5i[0]--p5i[4], erasebg=false, arrow);
draw(RRV, p5i[4]--p5i[3]--p5i[2]--p5o[2]);
drawPhoton(RRV, p5i[1]--p5i[2], erasebg=false);
drawPhoton(RRV, p5i[0]--p5o[0], erasebg=false, massive);
drawPhoton(RRV, p5i[4]--p5o[4], erasebg=false);
drawPhoton(RRV, p5i[3]--p5o[3], erasebg=false);

picture VV;
size(VV, w);
drawFermion(VV, p4ol[1]--p4il[1]--p4il[0]--p3i[0], erasebg=false, arrow);
draw(VV, p3i[0]--p3i[2]--p4il[2]--p4ol[2]);
drawPhoton(VV, p3i[1]--p3i[2], erasebg=false);
drawPhoton(VV, p4il[1]--p4il[2], erasebg=false);
drawPhoton(VV, p3i[0]--p3o[0], erasebg=false, massive);

picture RVV;
size(RVV, w);
drawFermion(RVV, p4oa[1]--p4ia[1]--p4ia[0]--p4i[0]--p4i[3]--p4i[2]--p4ia[2]--p4oa[2], erasebg=false, arrow);
drawPhoton(RVV, p4i[1]--p4i[2], erasebg=false);
drawPhoton(RVV, p4ia[1]--p4ia[2], erasebg=false);
drawPhoton(RVV, p4i[0]--p4o[0], erasebg=false, massive);
drawPhoton(RVV, p4i[3]--p4o[3], erasebg=false);

picture VVV;
size(VVV, w);
draw(VVV, p4oll[1]--p4ill[1]);
drawFermion(VVV, p4ill[1]--p4il[1]--p4il[0]--p3i[0], erasebg=false, arrow);
draw(VVV, p3i[0]--p3i[2]--p4il[2]--p4ill[2]--p4oll[2]);
drawPhoton(VVV, p3i[1]--p3i[2], erasebg=false);
drawPhoton(VVV, p4il[1]--p4il[2], erasebg=false);
drawPhoton(VVV, p4ill[1]--p4ill[2], erasebg=false);
drawPhoton(VVV, p3i[0]--p3o[0], erasebg=false, massive);

picture B2;
size(B2, 2*w, w);
add(B2, B.fit(), o);
label(B2, "$^2$", (l,l/2));

picture R2;
size(R2, 2*w, w);
add(R2, R.fit(), o);
label(R2, "$^2$", (l,l/2));

picture RR2;
size(RR2, 2*w, w);
add(RR2, RR.fit(), o);
label(RR2, "$^2$", (2*l/3,l/2));

picture RRR2;
size(RRR2, 2*w, w);
add(RRR2, RRR.fit(), o);
label(RRR2, "$^2$", (2*l/3,l/2));

picture V2;
size(V2, 2*w, w);
add(V2, B.fit(), (-l,0));
label(V2, "$\cdot$", o);
add(V2, V.fit(), (3/4*l,0));

picture RV2;
size(RV2, 2*w, w);
add(RV2, R.fit(), (-l,0));
label(RV2, "$\cdot$", o);
add(RV2, scale(1)*RV.fit(), (l,0));

picture RRV2;
size(RRV2, 2*w, w);
add(RRV2, RR.fit(), (-4*l/3,0));
label(RRV2, "$\cdot$", o);
add(RRV2, RRV.fit(), (l,0));

picture VV2;
size(VV2, 2*w, w);
add(VV2, scale(3/4)*B.fit(), (-3*l/4,0));
label(VV2, "$\cdot$", o);
add(VV2, VV.fit(), (l,0));
label(VV2, scale(0.5)*"$+\ldots$", (l,-l/2));

picture RVV2;
size(RVV2, 2*w, w);
fill(RVV2, (-2,15)--(34,15)--(34,-15)--(-2,-15)--cycle, opacity(0.2)+blue);
add(RVV2, scale(3/4)*R.fit(), (-3*l/4,0));
label(RVV2, "$\cdot$", o);
add(RVV2, RVV.fit(), (l,0));

picture VVV2;
size(VVV2, 2*w, w);
add(VVV2, scale(3/4)*B.fit(), (-3*l/4,0));
label(VVV2, "$\cdot$", o);
add(VVV2, VVV.fit(), (l,0));
label(VVV2, scale(0.5)*"$+\ldots$", (l,-l/2));

add(currentpicture, B2.fit(), graph_coord(-0.1,0));
add(currentpicture, R2.fit(), graph_coord(1,0));
add(currentpicture, RR2.fit(), graph_coord(2,0));
add(currentpicture, RRR2.fit(), graph_coord(3,0));

add(currentpicture, V2.fit(), graph_coord(0,1));
add(currentpicture, RV2.fit(), graph_coord(1,1));
add(currentpicture, RRV2.fit(), graph_coord(2,1));

add(currentpicture, VV2.fit(), graph_coord(0,2));
add(currentpicture, RVV2.fit(), graph_coord(1,2));

add(currentpicture, VVV2.fit(), graph_coord(0,3));
