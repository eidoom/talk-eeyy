import feynman;
import patterns;

int l = 20;
real phi = 1.618033988749,
     a = l,
		 b = l/phi,
		 d = l/(2*phi),
		 c = l/3+d/3,
		 e = 2.5;
path blob = scale(a, b)*unitcircle,
		 inner = scale(d)*unitcircle;
pair 
		 e1 = scale(a, b)*dir(225),
		 e2 = scale(a, b)*dir(315),
		 ys = scale(a, b)*dir(90);
pen massive = linewidth(2);
arrowbar arrow = MidArrow(TeXHead, 1.5);
add("hatch", hatch(1.5mm));

drawFermion(e*e1--e1, arrow);
drawFermion(e2--e*e2, arrow);
drawPhoton(ys--e*ys, massive);

filldraw(blob, white);
fill(blob, pattern("hatch"));

filldraw(shift(-c,0)*inner, white);
filldraw(shift(c,0)*inner, white);
