int x0 = 1,
    x1 = 8,
    y0 = 1,
    y1 = 4;

int xl = x1 - x0 + 2,
    yl = y1 - y0 + 2;

arrowbar arrow = Arrow(TeXHead);

draw((0, 0) .. (xl, 0), arrow);
draw((0, 0) .. (0, yl), arrow);

pair graph_coord(real x, real y) {
  return (x - x0 + 1, y - y0 + 1);
}

for(int i = 1; i < xl; ++i) {
  label((string) i, graph_coord(i, 0), S);
}

for(int i = 1; i < yl; ++i) {
  label((string) i, graph_coord(0, i), W);
}

label(rotate(90) * "loops (analytic)", (-1, yl / 2));
label("scales = ``legs + masses'' (algebraic)", (xl / 2, -1));

transform labelscale = scale(0.5);
int w = 60;

label(labelscale*minipage("\centering$2\to1$\\\scite{Lee:2022nhh,Chakraborty:2022yan}", w), graph_coord(1, 4));

label(labelscale*minipage("\centering$2\to1,1\text{m}$\\\scite{Fael:2022rgm,Egner:2022jot,Fael:2022miw,Fael:2023zqr}", w), graph_coord(1.5, 3));

label(labelscale*minipage("\centering$2\to2$\\\scite{Caola:2020dfu,Caola:2021rqz,Bargiela:2021wuy,Caola:2021izf,Caola:2022dfa}", w), graph_coord(2.5, 3));

label(labelscale*minipage("\centering$2\to2,1\text{m}$\\\scite{Campbell:2021mlr,Bonciani:2021okt,Mandal:2022vju,Gehrmann:2022vuk,!paper}", w), graph_coord(3, 1.8));

// label(labelscale*minipage("\centering$2\to2,2\text{m}$\\\scite{}", w), graph_coord(4, 2));

label(labelscale*minipage("\centering$2\to3$\\\scite{Abreu:2019odu,DeLaurentis:2020qle,Abreu:2020cwb,Chawdhry:2020for,Agarwal:2021grm,Abreu:2021oya,Chawdhry:2021mkw,Agarwal:2021vdh,Badger:2021imn,Badger:2023mgf,Abreu:2023bdp}", w), graph_coord(5, 1.5));

label(labelscale*minipage("\centering$2\to3,1\text{m}$\\\scite{Hartanto:2019uvl,Badger:2021nhg,Badger:2021ega,Badger:2022ncb}", w), graph_coord(6, 2));

label(labelscale*minipage("\centering$2\to3,2\text{m}$\\\scite{Budge:2020oyl,Campbell:2022qpq}", w), graph_coord(7, 1));

label(labelscale*minipage("\centering$2\to4$\\\scite{Laurentis:2019bjh}", w), graph_coord(8, 1));

label(labelscale*minipage("\scriptsize Disclaimers:\begin{itemize}\item author's bias\item certainly incomplete\item very approximate\end{itemize}", 100), (-0.25, -0.5));
