# [talk-eeyy](https://gitlab.com/eidoom/talk-eeyy)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8019969.svg)](https://doi.org/10.5281/zenodo.8019969)

* [Live slides](https://eidoom.gitlab.io/talk-eeyy/slides.pdf)
* Conference: [Radiative corrections and Monte Carlo tools for low-energy hadronic cross sections in $e^+e^-$ collisions](https://indico.psi.ch/event/13708/)
    * [Contribution](https://indico.psi.ch/event/13708/contributions/43409/)
